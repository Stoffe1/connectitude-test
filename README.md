Connectitude Smart Home Test
============================

How to run the application
--------------------------
1. Install [Yarn](http://yarnpkg.com)
2. Clone the [repositiory](https://bitbucket.org/Stoffe1/connectitude-test)
3. Using cli, ```cd``` to the backend folder and run ```yarn```. Wait for the process to complete.
4. Run ```yarn start``` to start the backend application
5. In a new cli window/tab ```cd``` to the frontend folder and run ```yarn```. Wait for the process to complete.
6. Run ```yarn start``` to start the frontend application
