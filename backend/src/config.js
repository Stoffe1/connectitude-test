module.exports = {
  /**
   * The port that the backend express server should listen on
   */
  port: 3001,

  allowedOrigins: [
    'http://localhost:3000',
    'http://127.0.0.1:3000',
  ],
};
