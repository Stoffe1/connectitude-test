const config = require('./config');

/**
 * Checks if the requesting origin is allowed to access the api.
 * If not send 401, if yes add cors headers and proceed to next
 *
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @returns
 */
function cors(req, res, next) {
  const origin = req.get('origin');

  if (!origin || !config.allowedOrigins.includes(origin)) {
    res.status(401);
    return res.send({ errorCode: 401, message: 'Unauthorized' });
  }

  res.header('Access-Control-Allow-Origin', origin);
  res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept');
  return next();
}

module.exports = cors;
