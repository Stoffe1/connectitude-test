const express = require('express');
const config = require('./config');
const cors = require('./cors');
const v1 = require('./v1');

// Initializes express
const app = express();

// Check cors as middlewear for every request and for option requests
app.use(cors);
app.options('/*', cors);

// Welcome message when requesting the root
app.get('/', (req, res) => {
  res.json({ message: 'Welcome to the smart home api.' });
});

// Version 1 router
app.use('/v1', v1);

// Start listen
app.listen(config.port, () => {
  console.log(`Listening on port ${config.port}`);
});
