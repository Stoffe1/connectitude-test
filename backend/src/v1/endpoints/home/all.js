const HomesModel = require('../../models/Homes');

/**
 * Gets a list of all home identifiers sorted alphabetically
 *
 * @param {object} req
 * @param {object} res¨
 * @returns {string}
 */
function getAllHomes(req, res) {
  return res.json(HomesModel.getAll().sort());
}

module.exports = getAllHomes;
