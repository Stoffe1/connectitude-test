const HomesModel = require('../../../models/Homes');

/**
 * Checks if a home identifier exist in the list of homes
 * If not it will send 404 response to the client.
 *
 * @param {string} homeId The id of the home to look for
 */
function homeExist(req, res, next) {
  if (!HomesModel.get(req.params.homeid)) {
    res.status(404);
    return res.send({ errorCode: 404, message: `Home with identifier ${req.params.homeid} does not exist` });
  }

  return next();
}

module.exports = homeExist;
