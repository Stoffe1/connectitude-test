const HomesModel = require('../../models/Homes');

/**
 * Generates a random float for temperature
 *
 * @param {number} [min=10]
 * @param {number} [max=20]
 * @returns {float}
 */
function randomTemperature(min = 10, max = 20) {
  const randomFloat = (Math.random() * (max - min)) + min;
  return parseFloat(randomFloat.toFixed(2));
}

/**
 * Generates a random float for humidity
 *
 * @returns {float}
 */
function randomHumidity() {
  const min = 0;
  const max = 1;

  const randomFloat = (Math.random() * (max - min)) + min;
  return parseFloat(randomFloat.toFixed(2));
}

/**
 * Get a specific home
 *
 * @param {any} req
 * @param {any} res
 */
function getHome(req, res) {
  const home = HomesModel.get(req.params.homeid);
  console.log(home);

  // If the request includes query parameter "randomize" the values for temperature and humidity
  // will be randomized, this exist in demo purpose
  if ('randomize' in req.query) {
    console.log('Note: Using random temperature and humidity values');
    home.rooms.map((input) => {
      const output = input;
      output.temperature = randomTemperature();
      output.humidity = randomHumidity();
      return output;
    });
  }

  res.json(home);
}

module.exports = getHome;
