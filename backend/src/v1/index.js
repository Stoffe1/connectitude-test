const express = require('express');
const homeExist = require('./endpoints/home/middlewear/homeExist');
const homeSingle = require('./endpoints/home/single');
const homeAll = require('./endpoints/home/all');

const router = express.Router();

/**
 * Lists all home identifiers sorted alphabetically
 */
router.get('/homes', homeAll);

/**
 * Gets the data for a specific home
 */
router.get('/homes/:homeid/data', homeExist, homeSingle);

module.exports = router;
