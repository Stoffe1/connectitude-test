const mockData = require('../../mock-data.json');
const Home = require('../objects/Home');

/**
 * Homes model that handles data layer communication.
 * For now we're using a mocked data layer (mock-data.json)
 *
 * @class Homes
 */
class Homes {
  /**
   * Get all homes sorted alphabetically
   *
   * @returns {Array.<Home>} Array of homes
   * @memberof Homes
   */
  getAll() {
    return Object.keys(mockData);
  }

  /**
   * Get a specific home by id
   *
   * @param {string} id
   * @returns {Home}
   * @memberof Homes
   */
  get(id) {
    if (!mockData[id]) {
      return null;
    }

    return new Home(mockData[id]);
  }
}

module.exports = new Homes();
