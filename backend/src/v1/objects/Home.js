const Room = require('../objects/Room');

/**
 * Defines the Home object
 *
 * @class Home
 */
class Home {
  /**
   * Creates an instance of Home.
   * @param {Object} home
   * @param {Array} home.rooms
   * @memberof Home
   */
  constructor({ rooms }) {
    this.rooms = this.parseRooms(rooms);
  }

  parseRooms(rooms) {
    return rooms.map((room) => new Room(room));
  }
}

module.exports = Home;
