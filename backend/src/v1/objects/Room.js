/**
 * Defines the Room object
 *
 * @class Room
 */
class Room {
  constructor({
    name, temperature, humidity, icon,
  }) {
    this.name = name;
    this.temperature = temperature;
    this.humidity = humidity;
    this.icon = icon;
  }
}

module.exports = Room;
