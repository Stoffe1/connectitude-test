import React, { PureComponent } from 'react';
import { Route, Switch } from 'react-router';
import { HashRouter } from 'react-router-dom';
import Home from './Pages/Home';
import Homes from './Pages/Homes';
import NotFound from './Pages/NotFound';

class App extends PureComponent {
  /** @inheritDoc */
  render() {
    return (
      <HashRouter basename="/">
        <Switch>
          <Route
            exact
            path="/"
            component={Homes}
          />
          <Route
            path="/home/:homeId/"
            component={Home}
          />
          <Route component={NotFound} status={404} />
        </Switch>
      </HashRouter>
    );
  }
}

export default App;
