import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './ErrorMessage.css';

class ErrorMessage extends PureComponent {
  /** @inheritDoc */
  render() {
    return (
      <div className="error-message">
        <img src="https://media1.giphy.com/media/DvyLQztQwmyAM/giphy.gif" alt="404 not found" />
        <article>
          <h1>{this.props.code}</h1>
          <h3>{this.props.message}</h3>
        </article>
      </div>
    );
  }
}

ErrorMessage.propTypes = {
  code: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

export default ErrorMessage;
