import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Statistic from '../Statistic/Statistic';

import './Room.css';

class Room extends PureComponent {
  /** @inheritDoc */
  render() {
    return (
      <div className="room">
        {this.props.icon && <img className="room-icon" src={this.props.icon} alt={this.props.name} />}
        <h1 className="room-name">{this.props.name}</h1>
        <div className="room-values">
          <Statistic label="Temperature" value={this.props.temperature} unit="°C" />
          <Statistic label="Humidity" value={Math.round(this.props.humidity * 100).toFixed(0)} unit="%" />
        </div>
      </div>
    );
  }
}

Room.propTypes = {
  name: PropTypes.string.isRequired,
  temperature: PropTypes.number.isRequired,
  humidity: PropTypes.number.isRequired,
  icon: PropTypes.string,
};

Room.defaultProps = {
  icon: null,
};

export default Room;
