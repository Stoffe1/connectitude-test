import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import String from '../../Utilities/String';
import './RoomList.css';

class RoomList extends PureComponent {
  /** @inheritDoc */
  render() {
    return (
      <ul className="room-list">
        {this.props.rooms.map(room => (
          <li key={`link-${room.name}`}>
            <Link to={`${this.props.baseUrl}/${String.kebabCase(room.name)}`}>
              {room.name}
            </Link>
          </li>
        ))}
      </ul>
    );
  }
}

RoomList.propTypes = {
  rooms: PropTypes.arrayOf(PropTypes.object).isRequired,
  baseUrl: PropTypes.string.isRequired,
};

export default RoomList;
