import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './Statistic.css';

class Statistic extends PureComponent {
  /** @inheritDoc */
  render() {
    return (
      <div className="statistic">
        <div className="statistic-label">{this.props.label}</div>
        <div className="statistic-container">
          <span className="statistic-value">{this.props.value}</span>
          <span className="statistic-unit">{this.props.unit}</span>
        </div>
      </div>
    );
  }
}

Statistic.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  unit: PropTypes.string.isRequired,
};

export default Statistic;
