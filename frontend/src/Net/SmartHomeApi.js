import String from '../Utilities/String';

const Api = {
  url: 'http://localhost:3001/v1',
  endpoints: {
    homes: {
      path: '/homes',
      method: 'GET',
    },
    home: {
      path: '/homes/{homeId}/data?randomize',
      method: 'GET',
    },
  },
};

class SmartHomeApi {
  /**
   * Gets/generates full endpoint url
   *
   * @static
   * @param {String} endpoint
   * @param {Object} [params={}]
   * @returns {String}
   * @memberof SmartHomeApi
   */
  static getUrl(endpoint, params = {}) {
    return String.injectParams(`${Api.url}${endpoint}`, params);
  }


  /**
   * Makes get requests to get list of homes
   *
   * @static
   * @returns {Array} List of homes
   * @memberof SmartHomeApi
   */
  static getHomes() {
    return fetch(SmartHomeApi.getUrl(Api.endpoints.homes.path))
      .then((result) => {
        if (result.status !== 200) {
          return Promise.reject(result);
        }

        return result;
      })
      .then(result => result.json());
  }

  /**
   * Makes a get request to get a specific home
   *
   * @static
   * @param {String} homeId
   * @returns {Object}
   * @memberof SmartHomeApi
   */
  static getHome(homeId) {
    if (!homeId) {
      return Promise.reject(new Error('No homeId given'));
    }

    return fetch(SmartHomeApi.getUrl(Api.endpoints.home.path, { homeId }))
      .then((result) => {
        if (result.status !== 200) {
          return Promise.reject(new Error('Home not found'));
        }

        return result;
      })
      .then(result => result.json());
  }
}

export default SmartHomeApi;
