import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';
import String from '../Utilities/String';
import Room from '../Components/Room/Room';
import RoomList from '../Components/RoomList/RoomList';
import SmartHomeApi from '../Net/SmartHomeApi';
import ErrorMessage from '../Components/ErrorMessage/ErrorMessage';

class Home extends PureComponent {
  /**
   * Creates an instance of Home.
   * @memberof Home
   */
  constructor() {
    super();
    this.updateInterval = null;
    this.getRooms = this.getRooms.bind(this);
    this.state = {
      error: false,
      rooms: null,
    };
  }

  /** @inheritDoc */
  componentWillMount() {
    this.getRooms();
    this.updateInterval = setInterval(this.getRooms, 10000);
  }

  /**
   * Get rooms data
   *
   * @memberof Home
   */
  getRooms() {
    SmartHomeApi.getHome(this.props.match.params.homeId)
      .then((result) => {
        this.setState({
          rooms: result.rooms,
        });
      })
      .catch(error => this.setState({ error }));
  }

  /**
   * Renders not found error message
   *
   * @returns {jsx}
   * @memberof Home
   */
  renderNotFound() {
    return (
      <ErrorMessage code="404" message="Not found" />
    );
  }

  /** @inheritDoc */
  render() {
    if (this.state.error) {
      return this.renderNotFound();
    }

    if (!this.state.rooms) {
      return (
        <div>Loading...</div>
      );
    }

    return (
      <div className="page-home container">
        {this.state.rooms[0] && <Route
          exact
          path={`${this.props.match.url}`}
          key={`route-room-${String.kebabCase(this.state.rooms[0].name)}`}
          render={() => (
            <Room name={this.state.rooms[0].name} temperature={this.state.rooms[0].temperature} humidity={this.state.rooms[0].humidity} icon={this.state.rooms[0].icon} />
            )}
        />}

        {this.state.rooms.map(room => (
          <Route
            exact
            path={`${this.props.match.url}/${String.kebabCase(room.name)}`}
            key={`route-room-${String.kebabCase(room.name)}`}
            render={() => (
              <Room name={room.name} temperature={room.temperature} humidity={room.humidity} icon={room.icon || null} />
            )}
          />
        ))}

        <RoomList baseUrl={this.props.match.url} rooms={this.state.rooms} />
      </div>
    );
  }
}

Home.propTypes = {
  match: PropTypes.object.isRequired,
};

export default Home;
