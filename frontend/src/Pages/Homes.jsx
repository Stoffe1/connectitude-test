import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import String from '../Utilities/String';
import SmartHomeApi from '../Net/SmartHomeApi';

class Homes extends PureComponent {
  /**
   * Creates an instance of Home.
   * @memberof Home
   */
  constructor() {
    super();
    this.state = {
      homes: null,
    };
  }

  /** @inheritDoc */
  componentWillMount() {
    this.getHomes();
  }

  /**
   * Get rooms data
   *
   * @memberof Home
   */
  getHomes() {
    SmartHomeApi.getHomes()
      .then((result) => {
        this.setState({
          homes: result,
        });
      });
  }

  /** @inheritDoc */
  render() {
    if (!this.state.homes) {
      return (
        <div>Loading...</div>
      );
    }

    return (
      <div className="page-homes container">
        <h2>Select home</h2>
        {this.state.homes &&
          <ul className="room-list">
            { this.state.homes.map(home => (
              <li><Link to={`/home/${String.kebabCase(home)}`}>{home}</Link></li>
            )) }
          </ul>
        }
      </div>
    );
  }
}

export default Homes;
