import React, { PureComponent } from 'react';
import ErrorMessage from '../Components/ErrorMessage/ErrorMessage';

class NotFound extends PureComponent {
  /** @inheritDoc */
  render() {
    return (
      <ErrorMessage code="404" message="Not found" />
    );
  }
}

export default NotFound;
