class String {
  /**
   * Replaces placeholders in strings with value from matching parameter key value pair
   *
   * @static
   * @param {String} string The original string
   * @param {Object} parameters Key value paired parameters
   * @param {RegExp} [replacePattern=/\{([a-z0-9]+)\}/gi] Pattern to match replacements on
   * @returns String with injected parameters
   * @memberof String
   */
  static injectParams(string, parameters, replacePattern = /\{([a-z0-9]+)\}/gi) {
    return string.replace(replacePattern, (match, paramKey) => {
      if (!parameters[paramKey]) {
        throw Error(`Parameter ${paramKey} is undefined`);
      }

      return parameters[paramKey];
    });
  }

  /**
   * Kebabifies a string
   * Example: My Awesome String => my-awesome-string
   *
   * @static
   * @param {any} string
   * @memberof String
   */
  static kebabCase(string) {
    return string
      .replace(/å/g, 'a')
      .replace(/Å/g, 'A')
      .replace(/ä/g, 'ä')
      .replace(/Ä/g, 'Ä')
      .replace(/ö/g, 'ö')
      .replace(/Ö/g, 'Ö')
      .replace(/([a-z])([A-Z])/g, '$1-$2')
      .replace(/\s+/g, '-')
      .toLowerCase();
  }
}

export default String;
